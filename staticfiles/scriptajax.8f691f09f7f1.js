function createSmth(event, title) {
    event.preventDefault(true)
    fetch('http://localhost:8000/story9/', {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-CSRFToken': document.getElementsByName('csrfmiddlewaretoken')[0].value,
        'X-Requested-With': 'XMLHttpRequest',
      },  
      body: JSON.stringify({
          title: title,
          count: document.getElementById('count').innerText,
      })
    }).then(function(response) {
        return response.json();
    }).then(function(data) {
        document.getElementById('count').innerText = data.count;
    }).catch(err => console.error(err));
  }
