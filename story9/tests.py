from django.test import TestCase
from django.test import Client
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from .views import buku

# Create your tests here.

class BukuPageTest(TestCase):
    def test_using_buku(self):
        found = resolve("/story9/")
        self.assertEqual(found.func, buku)

    def test_using_template(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'book.html')

    def test_message(self):
        response = Client().get('/story9/')
        self.assertContains(response, '0')

    def test_message2(self):
        response = Client().get('/story9/')
        self.assertContains(response, 'Title')
        self.assertContains(response, 'Publisher')
        self.assertContains(response, 'Published Date')

class URLTest(TestCase):
    def url_can_be_accessed(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

