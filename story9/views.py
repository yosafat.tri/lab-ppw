from django.shortcuts import render
import requests
import json
from django.http import JsonResponse
# Create your views here.
def buku(request):
    if request.method == 'POST' and request.is_ajax():
        adata = json.loads(request.body)
        new_count = int(adata['count']) + 1
        return JsonResponse({'count' : str(new_count)})
    else:
        apis = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting")
        bukus = apis.json()
        return render(request, 'book.html', {"bukus" : bukus})

