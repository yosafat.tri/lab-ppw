from django.conf.urls import url
from .views import *

app_name = 'lab4'
urlpatterns = [
    url(r'^$', firstdesign, name="firstdesign"),
    url(r'^biodata$', biodata, name="biodata"),
    url(r'^form$', form, name="form"),
    url(r'^jadwal/$', jadwal, name="jadwal"),
    url(r'^createPost$', createPost, name="createPost"),
    url(r'^delete$', delete, name="delete"),
    url(r'^subs', subs, name='subs'),
]
