from django.shortcuts import render, redirect
from .forms import Status_form
from .models import Status
from datetime import datetime

def landing(request):
    statuses = Status.objects.all().values()
    form = Status_form(request.POST)
    if form.is_valid():
        status = Status()
        status.post = form.cleaned_data['post']
        status.time = datetime.now()
        status.save()
        form = Status_form()
    return render(request, 'landing.html', {'form' : form, 'statuses':statuses})

def profile(request):
    response = {}
    return render(request, 'profile.html', response)

def belong(request):
    response = {}
    return render(request, 'belongings.html', response)