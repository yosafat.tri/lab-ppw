from django import forms

class Status_form(forms.Form):
    post =  forms.CharField(max_length=300, widget=forms.TextInput(attrs={
        "class":"fields",
        "required":True,
        "placeholder":"What do you think?"
    }))
