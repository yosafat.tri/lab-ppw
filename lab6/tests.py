from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import landing, profile
from .models import Status
from .forms import Status_form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest
import time
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class LandingPageTest(TestCase):
    def test_using_landing(self):
        found = resolve('/lab6/')
        self.assertEqual(found.func, landing)

    def test_using_template(self):
        response = Client().get('/lab6/')
        self.assertTemplateUsed(response, 'landing.html')

    def test_landing_message(self):
        response = Client().get('/lab6/')
        self.assertContains(response, 'Hello, ')
        self.assertContains(response, 'Apa kabar?')

    def test_landing_message2(self):
        response = Client().get('/lab6/')
        self.assertContains(response, 'What do you feel?')

class Url_test(TestCase):
    def url_can_be_accessed(self):
        response = Client().get('/lab6/')
        self.assertEqual(response.status_code, 200)

    def url_can_be_accessed2(self):
        response = Client().get('/lab6/profile/')
        self.assertEqual(response.status_code, 200)

    def url_can_be_accessed3(self):
        response = Client().get('/lab6/belong/')
        self.assertEqual(response.status_code, 200)

class StatusModelTest(TestCase):
    def test_default_attribute(self):
        status = Status()
        self.assertEqual(status.post, '')

class ProfilePageTest(TestCase):
    def test_using_profile(self):
        found = resolve('/lab6/profile/')
        self.assertEqual(found.func, profile)

    def test_profile_using_template(self):
        response = Client().get('/lab6/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_profile_message(self):
        response = Client().get('/lab6/profile/')
        self.assertContains(response, "Hello, I'm Yosafat Tri Putra Brata")
        self.assertContains(response, 'This is my website')
        
    def test_profile_message2(self):
        response = Client().get('/lab6/profile/')
        self.assertContains(response, 'Name : Yosafat Tri Putra Brata')
        self.assertContains(response, 'NPM : 1706043430')
        self.assertContains(response, 'Description : ')

    def test_profile_message3(self):
        response = Client().get('/lab6/profile/')
        self.assertContains(response, 'Track Record :')
        self.assertContains(response, 'Graduated from :')
        self.assertContains(response, 'Currently studying at :')
        self.assertContains(response, 'Skills :')

    def test_profile_message4(self):
        response = Client().get('/lab6/profile/')
        self.assertContains(response, 'What I Like:')

    def test_profile_message5(self):
        response = Client().get('/lab6/profile/')
        self.assertContains(response, '(C)Copyright 2018 Yosafat Tri Putra Brata')

# class FormFieldTest(unittest.TestCase):
#     def setUp(self):
#         options = Options()
#         options.add_argument('--headless')
#         options.add_argument('--no-sandbox')
#         options.add_argument('--disable-dev-shm-usage')
#         self.browser = webdriver.Chrome(chrome_options=options)

#     def test_form(self):
#         selenium = self.browser

#         selenium.get('http://localhost:8000/lab6/')

#         test = selenium.find_elements_by_class_name('test')
#         self.assertTrue(test != None)
        
#         inputfield = selenium.find_element_by_xpath("//*[@id='id_post']")
#         submitbutton = selenium.find_element_by_xpath("/html/body/section[1]/div/section/form/input[2]")
#         inputfield.send_keys('Coba Coba')
#         submitbutton.submit()
        

#         feed = selenium.find_elements_by_class_name('statusfield2')
#         self.assertIn('Coba Coba', selenium.page_source)

#     def tearDown(self):
#         self.browser.implicitly_wait(3)
#         self.browser.quit()

# class LayoutTest(unittest.TestCase):
#     def setUp(self):
#         options = Options()
#         options.add_argument('--headless')
#         options.add_argument('--no-sandbox')
#         options.add_argument('--disable-dev-shm-usage')
#         self.browser = webdriver.Chrome(chrome_options=options)

#     def tearDown(self):
#         self.browser.implicitly_wait(3)
#         self.browser.quit()

#     def test_layout(self):
#         selenium = self.browser

#         selenium.get('http://localhost:8000/lab6/profile/')

#         header = selenium.find_element_by_tag_name('header')
#         header_attribute = header.get_attribute('innerHTML')
#         assert 'This is my website' in header_attribute

#     def test_layout2(self):
#         selenium = self.browser
        
#         selenium.get('http://localhost:8000/lab6/profile/')

#         title = selenium.find_element_by_tag_name('Title')
#         title_attribute = title.get_attribute('innerHTML')
#         self.assertEqual(title_attribute, 'Profile')

# class CSSTest(unittest.TestCase):
#     def setUp(self):
#         options = Options()
#         options.add_argument('--headless')
#         options.add_argument('--no-sandbox')
#         options.add_argument('--disable-dev-shm-usage')
#         self.browser = webdriver.Chrome(chrome_options=options)

#     def tearDown(self):
#         self.browser.implicitly_wait(3)
#         self.browser.quit()

#     def test_css(self):
#         selenium = self.browser

#         selenium.get('http://localhost:8000/lab6/profile/')

#         like = selenium.find_element_by_class_name("like")
#         like_css = like.value_of_css_property('padding-top')
#         self.assertEqual(like_css, '32px')

#     def test_css2(self):
#         selenium = self.browser

#         selenium.get('http://localhost:8000/lab6/profile/')

#         like = selenium.find_element_by_class_name("retro")
#         like_css = like.value_of_css_property('padding-left')
#         self.assertEqual(like_css, '15px')

        
